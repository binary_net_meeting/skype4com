﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using SKYPE4COMLib;

namespace skypetest
{
    public partial class Form1 : Form
    {
        private Skype _Skype;
        private readonly Random _Random = new Random();
        private readonly List<string> _QuestionBegins = new List<string>
                                                        {
                                                            "COULD",
                                                            "HAVE",
                                                            "DO",
                                                            "DOES",
                                                            "HAS",
                                                            "DONT",
                                                            "DON'T"
                                                        };

        private string _CurrentUser;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _Skype = new Skype();
            _Skype.Attach();

            _CurrentUser = _Skype.CurrentUser.Handle;

            _Skype.MessageStatus += skypeOnMessageStatus;
        }

        private void skypeOnMessageStatus(ChatMessage message, TChatMessageStatus status)
        {
            if (message.Sender.Handle == _CurrentUser)
            {
                return;
            }

            if (string.IsNullOrEmpty(message.EditedBy))
            {
                return;
            }

            var text = message.Body.ToUpper();
            if (text.StartsWith("HI"))
            {
                Thread.Sleep(_Random.Next(3000));
                _Skype.SendMessage(message.Sender.Handle, "Hi");
                return;
            }

            if (text.EndsWith("?") || _QuestionBegins.Any(text.StartsWith))
            {
                Thread.Sleep(_Random.Next(3000));
                _Skype.SendMessage(message.Sender.Handle, "Let me 5 mins to check, please");
            }
        }
    }
}
